<?php

$cmd = 'getlist';
if (isset($_GET['cmd'])) {
    $cmd = $_GET['cmd'];
}

if ($cmd === 'book-list') {
    require_once "book-list.php";
}
else if ($cmd === 'book-form') {
    require 'book-add.php';
}
else if ($cmd === 'book-add-submit') {
    require_once 'book-add-submit.php';
}
else if ($cmd === "book-edit") {
    require_once 'book-edit.php';
}
else if ($cmd === 'book-edit-submit') {
    require_once 'book-edit-submit.php';
}
else if ($cmd === 'author-list') {
    require_once 'author-list.php';
}
else if ($cmd === 'author-edit') {
    require_once 'author-edit.php';
}
else if ($cmd === 'author-edit-submit') {
    require_once 'author-edit-submit.php';
}
else if ($cmd === 'author-form') {
    require_once 'author-add.php';
}
else if ($cmd === 'author-add-submit') {
    require_once 'author-add-submit.php';
}
else if ($cmd === 'getlist') {
    require_once "book-list.php";
}
else {
    http_response_code(400);
}

