<?php

include_once __DIR__ . '/BookDao.php';
include_once __DIR__ . '/Book.php';
include_once __DIR__ . '/AuthorDao.php';
include_once 'tpl.php';



$submitButton = $_POST['submitButton'] ?? '';
$book_id = $_POST['id'] ?? '';
$title = $_POST['title'] ?? '';
$grade = $_POST['grade'] ?? '';
$isReadTemp = intval($_POST['isRead']) ?? '';
$author1 = intval($_POST['author1']) ?? '';
$author2 = intval($_POST['author2']) ?? '';

$isRead = false;
if ($isReadTemp === 'on') {
    $isRead = true;
} else {
    $isRead = false;
}

$pageid = 'book-form-page';
$pageid1 = 'book-list-page';

$book = new Book($title, $grade, $isRead, $author1, $author2);

$bookdao = new BookDao();

if ($submitButton == 'Salvesta') {
    if (strlen($title) < 3 or strlen($title) > 23 ) {
        $authordao = new AuthorDao();
        $authors = $authordao->getAuthors();
        $message = "Pealkiri peab olema 3-23 tähemärki";
        $data = [
            'pageid' => $pageid,
            'title' => $title,
            'author1' => $author1,
            'author2' => $author2,
            'grade' => $grade,
            'isRead' => $isRead,
            'error' => $message,
            "authors" => $authors,
            'template' => 'book-add.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    } else {
        $bookdao->saveBook($book);
        header("Location: ?cmd=book-list&message=saved");
        die();

    }
}
