<?php

include_once __DIR__ . '/AuthorDao.php';
include_once __DIR__ . '/Author.php';
include_once 'tpl.php';

$tempGrade = $_POST['grade'] ?? '';

if (is_integer($tempGrade)) {
    $grade = $tempGrade;
} else $grade = '';

$firstName = $_POST['firstName'] ?? '';
$lastName = $_POST['lastName'] ?? '';
$grade = $_POST['grade'] ?? '';

$author = new Author($firstName, $lastName, $grade);
$authordao = new AuthorDao();
$pageid = 'author-form-page';
$pageid1 = 'author-list-page';

if (isset($_POST['submitButton'])) {
    if (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22) {
        $message = 'Eesnimi peab olema 1 kuni 21 tähemärki ja perekonnanimi peab olema 2 kuni 22 tähemärki!';
        $data = [
            'pageid' => $pageid,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'grade' => $grade,
            'error' => $message,
            'template' => 'author-add.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    } else {
        $authordao->saveAuthor($author);
        header("Location: ?cmd=author-list&message=saved");
        die();
    }
}
