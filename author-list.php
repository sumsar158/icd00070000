<?php

include_once __DIR__ . '/AuthorDao.php';
include_once 'tpl.php';

$dao = new AuthorDao();
$authors = $dao->getAuthors();
$pageid = 'author-list-page';

$mes = '';
$message = '';

if (isset($_GET['message'])) {
    $mes = $_GET['message'];
}

if($mes === 'saved') {
    $message = "Salvestatud";
}
if($mes === 'updated') {
    $message = "Uuendatud";
}
if($mes === 'deleted') {
    $message = "Kustutatud";
}

$data = [

        'pageid' => $pageid,
        'authors' => $authors,
        'message' => $message,
        'template' => 'author-list.html'

];
print renderTemplate('tpl/main.html', $data);