<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/Contact.php';
$contacts = getContacts();

foreach ($contacts as $contact) {
    print $contact;
}

function getContacts() : array {
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, name, number from contact left join phone p on contact.id = p.contact_id');

    $stmt->execute();


    foreach ($stmt as $row) {
        $id = $row['id'];
        $name = $row['name'];
        $number = $row['number'];

        if (isset($contacts[$id])) {
            $contact = $contacts[$id];
        } else {
            $contact = new Contact($id, $name);
            $contacts[$id] = $contact;
        }
        if ($number !== null) {
            $contact->addPhone($number);

        }

    }

    return array_values($contacts);
}
